# Pokémon trainer

The last assignment in the frontend part of noroffs a .NET accelerate course is a pokèmon trainer single page application. Here you can log in and add pokémon to your collection. You can also display 

## Links

* [Component tree](https://www.figma.com/file/XCf1woIyi8XtFObXAchshl/AngularComponentTree?node-id=0%3A1)

* [App on Heroku](https://murmuring-anchorage-48301.herokuapp.com/)

## Installation

* Clone down the repository
* ``$npm install``
* ``$npm serve -o``
* go to localhost url

## Usage

* Enter your username
* Add or remove pokémon to your collection
* Watch your collection in the tainer page.

## Authors
[Ludvig Hansen](https://gitlab.com/Givdul) and [Martin Kanestrøm](https://gitlab.com/Martinkanestrom)

## License
[MIT](https://choosealicense.com/licenses/mit/)
