import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { FavoriteService } from 'src/app/services/favorite.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-favorite-button',
  templateUrl: './favorite-button.component.html',
  styleUrls: ['./favorite-button.component.css']
})
export class FavoriteButtonComponent implements OnInit {

  public isFavorite: boolean = false;

  @Input() pokemonName: string = "";

  get loading(): boolean {
    return this.favoriteService.loading;
  }

  constructor(
    private readonly favoriteService: FavoriteService,
    private readonly userService: UserService
  ) { }

  ngOnInit(): void {
    this.isFavorite = this.userService.inFavorites(this.pokemonName)
  }

  onFavoriteClick(): void {
    this.isFavorite = !this.isFavorite;
    this.favoriteService.addToFavorites(this.pokemonName)
    .subscribe({
      next: (response: User) => {
        console.log("Next", response);
      },
      error: (error: HttpErrorResponse) => {
        console.log("ERROR", error.message); 
      }
    })
  }
}
