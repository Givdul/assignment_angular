import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-pokemon-catalogue-list',
  templateUrl: './pokemon-catalogue-list.component.html',
  styleUrls: ['./pokemon-catalogue-list.component.css']
})
export class PokemonCatalogueListComponent implements OnInit {

  @Input() pokemon!: Pokemon;

  constructor() { }

  ngOnInit(): void {
  }

}
