import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonTrainerItemComponent } from './pokemon-trainer-item.component';

describe('PokemonTrainerItemComponent', () => {
  let component: PokemonTrainerItemComponent;
  let fixture: ComponentFixture<PokemonTrainerItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokemonTrainerItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonTrainerItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
