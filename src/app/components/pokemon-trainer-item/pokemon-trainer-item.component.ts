import { Component, Input } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-pokemon-trainer-item',
  templateUrl: './pokemon-trainer-item.component.html',
  styleUrls: ['./pokemon-trainer-item.component.css']
})
export class PokemonTrainerItemComponent {

  public isFavorite: boolean = false;
  @Input() pokemon!: Pokemon;

  constructor(
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
    this.isFavorite = this.userService.inFavorites(this.pokemon.name)
  }

}
