export interface PokemonObject {
    count: number;
    prev: string;
    next: string;
    results: any;
}
