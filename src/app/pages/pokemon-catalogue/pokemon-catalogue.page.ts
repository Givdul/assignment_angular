import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage implements OnInit {

  get pokemons(): Observable<Pokemon[]> {
    console.log(this.pokemonsService.pokemons);
    return this.pokemonsService.pokemons;
  }

  get error(): string {
    return this.pokemonsService.error;
  }

  constructor(
    private readonly pokemonsService: PokemonService
  ) { }

  ngOnInit(): void {
    this.pokemonsService.findAllPokemons();
  }

}
