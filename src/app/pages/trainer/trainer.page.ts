import { Component, OnInit } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { Observable } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage {

  
  get name(): string {  
    let username = (JSON.parse(sessionStorage.getItem("pokemon-user")!).username);
    console.log(username);
    
    return username;
  }

  get pokemons(): Observable<Pokemon[]> {
    console.log(this.pokemonsService.pokemons);
    return this.pokemonsService.pokemons;
  }

  get error(): string {
    return this.pokemonsService.error;
  }

  constructor(
    private readonly pokemonsService: PokemonService
  ) { }

  ngOnInit(): void {
    this.pokemonsService.findAllPokemons();
  }

  

}
