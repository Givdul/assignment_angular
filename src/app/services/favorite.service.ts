import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UserService } from './user.service';
import { User } from "src/app/models/user.model";
import { finalize, Observable, tap } from 'rxjs';


const { apiKey, apiUsers } = environment;

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {

  private _loading: boolean = false;

  get loading(): boolean {
    return this._loading
  }

  constructor(
    private readonly http: HttpClient,
    private readonly userService: UserService,
  ) { }


  public addToFavorites(pokemon: string): Observable<User> {
    
    if(!this.userService.user) {
      throw new Error("addToFavorites: There is no user")
    }

    const user: User = this.userService.user;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': apiKey
    })

    if(this.userService.inFavorites(pokemon)) {
      return this.http.patch<User>(`${apiUsers}/${user.id}`, {
        pokemon: user.pokemon.filter(e => e !== pokemon)
      }, {
        headers
      }).pipe(
        tap((updatedUser: User) => {
          this.userService.user = updatedUser;
        }),
        finalize(() => {
          this._loading = false;
        })
      )
    }

    this._loading = true;
    
    return this.http.patch<User>(`${apiUsers}/${user.id}`, {
      pokemon: [...user.pokemon, pokemon]
    }, {
      headers
    }).pipe(
      tap((updatedUser: User) => {
        this.userService.user = updatedUser;
      }),
      finalize(() => {
        this._loading = false;
      })
    )
  }
}
