import { HttpClient } from "@angular/common/http"
import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { environment } from "src/environments/environment";
import { map, Observable, BehaviorSubject } from "rxjs";
import { PokemonObject } from "../models/pokemon-object.model";

const { apiPokemon } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private readonly _pokemons: BehaviorSubject<Pokemon[]> = new BehaviorSubject<Pokemon[]>([]);
  private _error: string = "";
  private _loading: boolean = false;

  get pokemons(): Observable<Pokemon[]> {
    return this._pokemons;
  }

  get loading(): boolean {
    return this._loading;
  }

  get error(): string {
    return this._error;
  }

  constructor(private readonly http: HttpClient) { }

  public findAllPokemons(): void {
    this.http
      .get<PokemonObject>(apiPokemon)
      .pipe(map((data: PokemonObject) => data.results))
      .subscribe({
        next: (pokemons) => {
          pokemons.map((pokemon: { url: string; imageUrl: string; }) => {
            const id = pokemon.url.split('/').filter(Boolean).pop();
            pokemon.imageUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`;
          });
          this._pokemons.next(pokemons);
        },
        error: (error) => {
          console.log(error);
        }
      });
  }
}
