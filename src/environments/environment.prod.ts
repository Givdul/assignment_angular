export const environment = {
  production: true,
  apiUsers: "https://lh-noroff-api.herokuapp.com/trainers",
  apiPokemon: "https://pokeapi.co/api/v2/pokemon?limit=151&offset=0"
};
